/**
 * First example test suite in Scala running on TestNG
 */

import org.scalatest._
import scala.collection.mutable.ListBuffer
import org.testng.annotations._
import org.testng.Assert._
import collection.mutable.Stack

class ExampleTest extends FlatSpec with Matchers
{
  var stringBuilder: StringBuilder = _
  var listBuffer: ListBuffer[String] = _

  @BeforeMethod
  def initialize() {
    println("**** CALLING INTIALIZE ****")
    stringBuilder = new StringBuilder("ScalaTest is ")
    listBuffer = new ListBuffer[String]
  }

  // Test using TestNG assertions
  @Test
  def verfiyEasy() {
    stringBuilder.append("easy!")
    //helperMethod()
    assertEquals("ScalaTest is easy!", stringBuilder.toString())
    assertTrue(listBuffer.isEmpty)

    listBuffer += "sweet"

    try{
      "verbose".charAt(-1)
      fail()
    } catch {
      case e: StringIndexOutOfBoundsException =>
    }
  }


  // Test using ScalaTest assertions
  @Test
  def verifyFun() {
    stringBuilder.append("fuxn!")
    //helperMethod()
    assert(stringBuilder.toString() === "ScalaTest is fun!")
    assert(listBuffer.isEmpty)
    listBuffer+="sweeter"

    intercept[StringIndexOutOfBoundsException]{
      "concise".charAt(-1)
    }
  }


//  @Test
//  def stackPopShouldBeLIFO {
//    val stack = new Stack[Int]
//    stack.push(1)
//    stack.push(2)
//    stack.pop() should be (2)
//    stack.pop() should be (1)
//  }
//
  @Test
  def emptyStackPopShouldThrowException {
    val emptyStack = new Stack[Int]
    emptyStack.push(10)
    a[NoSuchElementException] should be thrownBy{
      emptyStack.pop()
    }
  }


  def helperMethod(){
    println("not doing anything special")
  }


}
