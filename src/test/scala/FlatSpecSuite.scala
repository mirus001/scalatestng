/**
 * POC for FlatSpec tests
 */

import collection.mutable.Stack
import org.scalatest._

class FlatSpecSuite extends FlatSpec with Matchers {

  "A stack" should "pop values in LIFO order" in {
    val stack = new Stack[Int]
    stack.push(1)
    stack.push(2)
    stack.pop() should be (2)
    stack.pop() should be (1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[Int]
    emptyStack.push(10)
    a[NoSuchElementException] should be thrownBy{
      emptyStack.pop()
    }
  }

}
