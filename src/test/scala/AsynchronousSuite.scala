/**
 * Example suite running asynchronous scala tests
 *
 * This suite will leverage the org.scalatest.concurrent.AsyncAssertions library to attempt to run multiple async non-blocking tests in parallel
 *
 */

import org.scalatest._
import concurrent.AsyncAssertions
import akka.actor._


class AsyncTestSuite extends FunSuite with Matchers with AsyncAssertions {



  /*
  case class Message(text: String)

  class Publisher extends Actor {

    @volatile private var handle: Message => Unit = { (msg) => }

    def registerHandler(f: Message => Unit) {
      handle = f
    }

    def act() {
      var done = false
      while (!done) {
        react {
          case msg: Message => handle(msg)
          case "Exit" => done = true
        }
      }
    }
  }

  test("example one") {

    val publisher = new Publisher
    val message = new Message("hi")
    val w = new Waiter

    publisher.start()

    publisher.registerHandler { msg =>
      w { msg should equal (message) }
      w.dismiss()
    }

    publisher ! message
    w.await()
    publisher ! "Exit"
  }
  */
}